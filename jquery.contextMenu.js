/*
 * jQuery contextMenu plugin
 *
 * Usage:
 * $(selector).contextMenu(options);
 *
 * Example:
 * $('.menu').contextMenu({
 *
 * });
 *
 */

;(function ( $, window, document, undefined ) {

    // Create the defaults once
    var pluginName = "contextMenu",

        defaults = {
			menuClass : "contextBoxMenu",	// container css class
			itemClass : "contextItem", 		// container item css class
			customMenuClass : "", 			// additional class for container
			triggerClass : "cMTrigger", 	// internal css class
			openClass : "cMActive", 		// internal css class
			appendTo: "body",				// menu append element
			scrollContainer: "body",		// if menu container is not appended to body, set parent scrolling element
			customHtml: null,				// set custom html menu {String}
			width: "auto",					// set width
			zIndex: '9999',					// set zIndex
			event: "click",					// set event for menu activation
			opened: false,					// set flag for default state of context menu opened/closed
			items: [
				{ title:"Option1", value:"", html:"", handler: null, customClass:'contextItem', icon: "" },
				{ title:"Option2", value:"", html:"", handler: null, customClass:'contextItem', icon: "" }
			] 								// menu items array
        },

		utils = (function(){
			return {
				createNode: function(html) {
					var div = document.createElement('div');
					div.innerHTML = html;
					return div.firstChild;
				}
			}

		})(),

		keys = {
			ESC: 27,
			TAB: 9,
			RETURN: 13,
			LEFT: 37,
			UP: 38,
			RIGHT: 39,
			DOWN: 40
		};

    // The actual plugin constructor
    function Plugin( element, options ) {

		this.element = element;
		this.$el = $(element);

		// set options
        this.options = $.extend({}, defaults, options);
        this.menu = this.options.items;
        this.menuHtml = "";
		this.menuSelector = "." + this.options.menuClass;

		// plugin params
        this._defaults = defaults;
        this._name = pluginName;
		this._id = this._name + '_' + Plugin.instance;

		// run constructor
        this.init();
    }

	//static
	Plugin.instance = 0;
	Plugin.isVisible = {};

    Plugin.prototype = {

		init : function () {
			var self = this;

			Plugin.instance += 1;

			this.contextMenu = utils.createNode('<div class="'+ this.options.menuClass +'"> </div>');
			this.$contextMenu = $(this.contextMenu);
			//custom menu class
			if (this.options.customMenuClass) {
				this.$contextMenu.addClass(this.options.customMenuClass);
			}

			//get global listener's flag
			this.globalListener = $(document).data(this._name + '.events.global');

			//set trigger class
			this.$el.addClass(this.options.triggerClass);

			//set menu width
			if (this.options.width !== 'auto') {
                this.$contextMenu.width(this.options.width);
            }

			//set visibility
			Plugin.isVisible[this._id] = false;

			//check if options are created
			if (this.menu && this.options.customHtml === null) {
				this.buildMenu();
			}

			//get custom html for context menu
			if (this.options.customHtml !== null) {
				this.menuHtml = this.options.customHtml;
				this.$contextMenu.html(this.menuHtml);
			}

			//menu default state opened/closed
			if (this.options.opened) {
				this.onClick();
			}



			// contextMenu trigger event
			this.$el.on('click.' + this._name, function ( ) { self.onClick(); } );

			// global events
			if (!this.globalListener) {

				$(document).on('click.' + this._name, function (e) {

					var $target = $(e.target), o = self.options;

					if (!$target.hasClass(o.menuClass) && !$target.hasClass(o.triggerClass)) {
						if (!$target.parent().hasClass(o.menuClass) && !$target.parent().hasClass(o.triggerClass))
							self.hideAll();
					}

				});
				if (this.options.scrollContainer) {
					$(document).on('scroll.' + this._name, this.options.scrollContainer, function(){
						self.hideAll();
					});
				}

				//set global handler's flag
				$(document).data(this._name + '.events.global', true);
			}

		},
		buildMenu: function(){
			var self = this;
			this.itemId = self.options.itemClass + '_id_' + Plugin.instance + '_';

			this.menuHtml +='<ul>';

			$.each(this.menu, function(index, item){

				self.menuHtml += '<li class="'+ self.options.itemClass +'"';
				self.menuHtml += ' data-id="' + self.itemId + index +'"';
				self.menuHtml += item.handler ? ' data-handler="'+ encodeURIComponent(escape(item.handler)) +'"' : '';
				self.menuHtml += item.value ? 'data-handler="'+ item.value +'"' : '';
				self.menuHtml += '><p>';
				self.menuHtml += item.icon ? '<i class="icon-' + item.icon + '"></i>' : '' ;
				self.menuHtml += item.title + '</p>';
				self.menuHtml += item.html ? item.html : '';
				self.menuHtml += '</li>';

			});

			this.menuHtml +='</ul>';

			this.$contextMenu.html(this.menuHtml);
		},
		attachItemEvent: function () {
			var self = this,
				items = self.options.items;

			// append custom params data
			this.appendData();

			// Listen for click event on contexMenu list:
            this.$contextMenu.on('click.'+ this._name , '.' + this.options.itemClass, function (event) {
				var handler = $(this).data('handler'),
					callback = null;


				// TODOcustom params
				if (handler) {
					handler = unescape(decodeURIComponent(handler));
					callback = handler.match(/function[^{]+\{([\s\S]*)\}$/)[1];
					try {
						new Function("event", "element", "context", callback)(event, this, self);
					} catch (e) {
						console.log(e.stack);
					}
				}
            });
			/*
			this.$contextMenu.find('li').each(function (i, el) {
				var handlerNew = items[i].handlerNew,
					context = items[i].handlerContext;

					console.log('handlerNew', handlerNew.apply(context, el));
				if (handlerNew && context) {
					$(el).off().on(self.options.event, function(event){
						handlerNew.call(context, this);
					});
				}

				console.log('el', el);
			});*/
		},
		removeItemEvent: function () {
			 this.$contextMenu.off('click.'+ this._name , '**');
		},

		appendData: function () {
			var self = this;
			$.each(this.menu, function(index, item){
				if (item.params) {
					$('[data-id=' + self.itemId + index + ']').data('params', item.params);
				}
			});
		},
		onClick: function(){

			if (!Plugin.isVisible[this._id]) {

				if ($(this.menuSelector).length > 0) {
					this.hideAll();
				}

				this.showMenu();

			} else {

				this.hideMenu();

			}

		},

		onRightClick: function(){

		},
		setPosition: function(){
			var self = this,
                offset = self.$el.offset(),
				$parent = $(self.options.appendTo),
				pageWidth = $("#application").width(),
				parentWidth = $parent.outerWidth(),
				parentOffset = $parent.offset(),
				cssMenu = {
					position: 'absolute',
					zIndex: self.options.zIndex
				};

				this.$contextMenu.css(cssMenu);


            // Don't adjust position if custom container has been specified:
            if (self.options.appendTo !== 'body') {

				$parent.css("position","relative");


				this.$contextMenu.show().css({
					top: ((offset.top - parentOffset.top) + self.$el.outerHeight()) + 'px',
					left: (offset.left - parentOffset.left) + 'px'
				});

				//detect offset
				if (this.detectPageOffset()) {
					//code
					this.$contextMenu.show().css({
						top: ((offset.top - parentOffset.top) + self.$el.outerHeight()) + 'px',
						right: (parentWidth - (offset.left - parentOffset.left)) - self.$el.outerWidth() + 'px',
						left: 'auto'
					}).addClass('arrow-right');
				}

                return;
            }

			// check if menu is visible
			if (this.$el.is(":visible")) {

				this.$contextMenu.show().css({
					top: (offset.top + self.$el.outerHeight()) + 'px',
					left: offset.left + 'px'
				});

			}


			//detect offset
			if (this.detectPageOffset()) {
				//code
				this.$contextMenu.show().css({
					top: (offset.top + self.$el.outerHeight()) + 'px',
					right: pageWidth - (offset.left + self.$el.outerWidth()) + 'px',
					left: 'auto'
				}).addClass('arrow-right');
			}

		},

		detectPageOffset: function(){
			var pageWidth = $("#application").width(),
				elWidth = this.options.width !== 'auto' ? this.options.width : this.$el.width();
				elLeft = this.$el.offset().left;

				return (elWidth + elLeft) > pageWidth;
		},
		showMenu: function(){
			this.$contextMenu.hide().appendTo(this.options.appendTo).width(this.options.width).fadeIn('fast');
			this.setPosition();

			if (this.options.customHtml == null)
				this.attachItemEvent();

			this.$el.addClass(this.options.openClass);
			Plugin.isVisible[this._id] = true;
		},
		hideMenu: function(){
			this.$contextMenu.hide().remove();
			this.removeItemEvent();
			this.$el.removeClass(this.options.openClass);
			Plugin.isVisible[this._id] = false;
		},
		hideAll: function(){
			var vis = Plugin.isVisible, i;
			this.removeItemEvent();
			$(this.menuSelector).hide().remove();
			$('.' + this.options.openClass).removeClass(this.options.openClass);
			for ( i in vis ) {
				if (vis.hasOwnProperty(i)) {
					vis[i] = false;
				}
			}

		}

    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if ( !$.data(this, "plugin_" + pluginName )) {
                $.data( this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    }

})( jQuery, window, document );